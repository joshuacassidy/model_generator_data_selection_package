import pymongo, itertools

def write_data(attributes, dataset_name):
    temp_arr = []
    for i in attributes:
        if not i.startswith('.'):
            temp_arr.append(i)
    attributes = temp_arr


    client = pymongo.MongoClient("mongodb://josh_josh:JoshThesisFrameworkNCI@87.44.4.246:27017/?compressors=disabled&gssapiServiceName=mongodb",authSource="thesis")
    db = client.thesis
    subset = []

    for i in range(2,5):
        for record in itertools.combinations(attributes, i):
            subset.append(record)

    count = 0

    for i in subset:
        count += 1
        record = {
            "dataset_name": dataset_name, 
            "labels": i
        }
        db.predictor_training_data.insert_one(record)

    client.close()