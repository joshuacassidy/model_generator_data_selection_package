import itertools, os, pymongo
import select_data_utils


attributes = os.listdir("cifar10_unstructured")

select_data_utils.write_data(attributes=attributes, dataset_name="cifar10")